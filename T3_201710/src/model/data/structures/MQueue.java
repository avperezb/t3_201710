package model.data.structures;

public class MQueue<T> implements IQueue<T> {

	private ListaEncadenada<T> queue;

	public MQueue() {

		queue = new ListaEncadenada<>();
	}

	public void enqueue ( T item ){

		queue.agregarAlFinal(item);
	}

	public T dequeue () {

		return (T) queue.dequeue();
	}

	public int size ()
	{
		return queue.size();
	}

	public boolean isEmpty() 
	{
		return queue.isEmpty(); 
	}
	
	public T darElemento(int pos)
	{
		return (T) queue.darElemento(pos);
	}

}
