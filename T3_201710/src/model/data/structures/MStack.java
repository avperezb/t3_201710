package model.data.structures;

public class MStack<T> implements IStack<T>{

	private ListaEncadenada<T> stack;

	public MStack()
	{
		stack = new ListaEncadenada<>();
	}

	public T pop() {

		return stack.pop();
	}

	public int size()
	{
		return stack.size();
	}

	public boolean isEmpty() {

		return stack.isEmpty();
	}

	public void push(T item) {

		stack.push(item);
	}

	public T darElemento(int pos) {

		return (T) stack.darElemento(pos);
	}

}