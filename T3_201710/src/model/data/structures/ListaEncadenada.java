
package model.data.structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

//El c�digo de operaciones b�sicas de una lista encadenada fue tomado de el c�digo ejemplo uniandes/collections/linear.

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primero;

	private int size;

	private NodoSencillo<T> ultimo;

	public ListaEncadenada ( ){

		primero = new NodoSencillo<>();
		ultimo = null;																					
		size = 0;

	}

	public boolean isEmpty()
	{
		boolean vacio = false;

		if ( primero.getItem() != null || primero == null )
		{
			vacio = true;
		}

		return vacio;

	}

	public int size(){

		return size;
	}

	public void push(T item){

		NodoSencillo<T> antiguoPrimero = primero;
		NodoSencillo<T> nuevo = new NodoSencillo<T>();
		nuevo.setItem(item);

		if(primero == null){
			primero = nuevo;
			nuevo.setNext(null);
			size++;
			return;
		}
		else
		{
			nuevo.setNext(antiguoPrimero);
			primero = nuevo;
			size++;
		}
	}

	public T pop() {

		T itemExtraido = null;

		if ( primero != null )
		{
			itemExtraido = primero.getItem();
			primero = primero.getNext();
			size--;
			return itemExtraido;

		}
		else {
			System.out.println("La pila est� vac�a");
		}

		return itemExtraido;

	}

	public void enqueue( T item )
	{
		NodoSencillo<T> antiguoUltimo = ultimo;
		NodoSencillo<T> nuevoUltimo = new NodoSencillo<T>();
		nuevoUltimo.setItem(item);
		nuevoUltimo.setNext(null);

		if ( primero == null )
		{
			primero = nuevoUltimo;
			size++;
		}
		else
		{
			antiguoUltimo.setNext(ultimo);
			size++;
		}
	}

	public T dequeue ()
	{
		T item = null;

		if ( primero != null )
		{
			item = primero.getItem();
			primero = primero.getNext();
			size--;
		}
		else{

			ultimo = null;
		}
		return item;
	}

	public NodoSencillo<T> getFirst() {

		return primero;
	}

	public void eliminarEnK(int k){

		NodoSencillo<T> anterior = null;

		if(k == 0){	    
			primero = primero.getNext();
			size--;
			return;
		}

		int i = 0;
		NodoSencillo<T> actual = primero;

		while (i < k && actual != null){

			anterior = actual;
			actual =actual.getNext();

			i++;
		}
		anterior.setNext(actual.getNext());
		size--;

	}
	private class IteradorM<T> implements Iterator<T>{

		NodoSencillo<T> actual;

		public IteradorM( NodoSencillo<T> primero ){

			actual = primero;
		}


		public boolean hasNext() {

			boolean tiene = false;

			if ( primero != null && primero.getNext()!=null)
			{
				tiene = true;	
			}
			// TODO Auto-generated method stub
			return tiene;
		}

		public T next() {

			if ( hasNext() )
			{
				actual = primero.getNext();
			}
			// TODO Auto-generated method stub
			return actual.getItem();
		}


		public void remove() {

			// TODO Auto-generated method stub

		}

	}

	//		@Override
	public void agregarAlFinal(T elem) {

		NodoSencillo<T> nuevo = new NodoSencillo<T>(null, elem);

		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			size++;
			return;
		}
		else
		{
			NodoSencillo<T> actual = primero;
			while ( actual != null ){

				if ( actual.getNext()== null)
				{
					actual.setNext(nuevo);
					actual = actual.getNext();
				}


				actual = actual.getNext();
			}
		}
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub

		NodoSencillo<T> actual = primero;
		int contador = 0;

		if ( primero!= null )
		{
			if (primero.getNext()== null && pos == 0)
			{
				return (T) primero.getItem();
			}

			else if (primero.getNext()!= null)
			{

				while ( contador < pos )
				{

					actual = actual.getNext();
					contador++;
				}
			}
		}

		return (T)  actual.getItem();
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}



	@Override
	public boolean avanzarSiguientePosicion() {

		boolean avanza = false;
		NodoSencillo<T> actual = primero;

		if ( actual!= null && actual.getNext() != null )
		{
			avanza = true;
			actual = actual.getNext();
		}

		return avanza;

		// TODO Auto-generated method stub
	}



	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorM<>(primero);
	}

	@Override
	public T darElementoPosicionActual() {
		T item = null;
		NodoSencillo<T> actual = primero;

		if ( actual != null )
		{
			item = actual.getItem();
		}
		return item;
	}

	public boolean retrocederPosicionAnterior(  ) {

		// TODO Auto-generated method stub
		NodoSencillo<T> indice = primero;
		NodoSencillo<T> actual = primero;
		while (indice != null)
		{
			if (indice.getNext() != null)
			{
				actual = indice;
				return true;
			}
			indice = indice.getNext();
		}
		return false;
	}

}
