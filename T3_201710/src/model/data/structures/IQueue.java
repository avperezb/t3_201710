package model.data.structures;

public interface IQueue<T> {

	public void enqueue(T item);
	public T dequeue();
	public int size();
	public boolean isEmpty();

}
