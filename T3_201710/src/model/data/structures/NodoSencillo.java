package model.data.structures;

public class NodoSencillo<T> {

	private NodoSencillo next;

	private T item;

	public NodoSencillo (){

		next = null;
		item = null;

	}

	public NodoSencillo ( NodoSencillo siguiente, T pItem ){

		next = siguiente;
		item = pItem;
	}


	public NodoSencillo getNext() {
		
		return next;
	}

	public void setNext(NodoSencillo next) {
		this.next = next;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}

}
