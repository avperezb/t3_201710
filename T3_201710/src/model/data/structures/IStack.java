package model.data.structures;

public interface IStack<T> {

	public void push(T item);
	public T pop();
	public int size();
	public boolean isEmpty();
	
}
