package model.logic;

import java.util.ArrayList;

import javax.xml.namespace.QName;

import model.data.structures.MQueue;
import model.data.structures.MStack;

public class ManejadorExpresiones {

	public final static String SUMA = "+";

	public final static String RESTA = "-";

	public final static String MULTIPLICACION = "*";

	public final static String DIVISION = "/";

	MStack stack = new MStack<>();

	public boolean expresionBienFormada(String pExpresion) {
		// TODO Auto-generated method stub

		char[] expresion = pExpresion.toCharArray();
		boolean bienFormada = false;

		if (cantidadPyC(pExpresion))
		{
			if (parentesisAyC(pExpresion))
			{
				if (!soloSimbolos(pExpresion))
				{
					if(bienCerrado(pExpresion))
					{
						bienFormada = true;
					}
				}
			}
		}

		if (bienFormada== true)
		{
			for (int i = 0; i < expresion.length; i++) {
				stack.push(expresion[i]);
			}
		}

		return bienFormada;

	}

	public boolean parentesisAyC(String expresion)
	{
		int contador = 0;

		char[] expresiones = expresion.toCharArray();

		for (int i = 0; i < expresiones.length-1; i++) {

			char actual = expresiones[i];
			char next = expresiones[i+1];

			if ( actual == '(' && next == ')')
			{
				contador++;				
			}
			if ( actual == '[' && next == ']')
			{
				contador++;
			}

		}
		if (contador != 0)
		{
			return false;
		}
		else
		{
			return true;			
		}

	}

	public boolean masDeUnSimboloSeguido(String expresion)
	{
		int contador = 0;

		char[] expresiones = expresion.toCharArray();

		for (int i = 0; i < expresiones.length-1; i++) {

			char actual = expresiones[i];
			char next = expresiones[i+1];

			if ( esSimbolo(actual) && esSimbolo(next) )
			{
				contador++;
			}
		}
		if (contador == '0')
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public boolean soloSimbolos(String expresion)
	{
		char[] expresiones = expresion.toCharArray();

		for (int i = 0; i < expresiones.length-1; i++) {

			char actual = expresiones[i];

			if(esNumero(actual))
			{
				return false;
			}			
		}
		return true;
	}

	public boolean esSimbolo (char expresion)
	{
		boolean es = false;

		if (String.valueOf(expresion).equals(SUMA))
		{
			es = true;
		}
		if (String.valueOf(expresion).equals(RESTA))
		{
			es = true;
		}
		if (String.valueOf(expresion).equals(MULTIPLICACION))
		{
			es = true;
		}
		if (String.valueOf(expresion).equals(DIVISION))
		{
			es = true;
		}

		return es;
	}

	public boolean bienCerrado (String expresion)
	{
		ArrayList parentesisDerechos = new ArrayList();
		ArrayList parentesisIzquierdos = new ArrayList();
		ArrayList corchetesDerechos = new ArrayList();
		ArrayList corchetesIzquierdos = new ArrayList();

		int contador = 0;

		char[] expresiones = expresion.toCharArray();

		for (int i = 0; i < expresiones.length-1; i++) {

			char actual = expresiones[i];

			if ( actual == '(')
			{
				parentesisIzquierdos.add(i);
			}
			if ( actual == ')')
			{
				parentesisDerechos.add(i);
			}
			if ( actual == '[')
			{
				corchetesIzquierdos.add(i);
			}
			if ( actual == ']')
			{
				corchetesDerechos.add(i);
			}
		}
		if ((Integer)corchetesIzquierdos.size() > (Integer) corchetesDerechos.size() )
		{
			if (parentesisDerechos.size() == parentesisIzquierdos.size())
			{
				return true;
			}
		}
		return false;

	}

	public boolean esNumero (char entrada)
	{
		return ( entrada > 47 && entrada < 58  );
	}


	public boolean esNumero2(String entrada){
		try{
			double numero = Double.parseDouble(entrada);
			return true;
		}
		catch(Exception e){
			return false;
		}		
	}

	public boolean cantidadPyC(String expresion)
	{
		//Verifica que la cantidad de paréntesis y corchetes sea par

		int contadorParentesis = 0;
		int contadorCorchetes = 0;
		int contadorPA = 0;
		int contadorC = 0;

		boolean esPar = false;

		char[] expresiones = expresion.toCharArray();

		for (int i = 0; i < expresiones.length; i++) {

			char actual = expresiones[i];

			if ( actual == '(' )
			{
				contadorParentesis++;
			}
			if ( actual == ')')
			{
				contadorParentesis++;
			}
			if ( actual == '[')
			{
				contadorCorchetes++;
			}
			if ( actual == ']')
			{
				contadorCorchetes++;
			}
		}

		if ( contadorCorchetes%2==0 || contadorParentesis%2==0 )
		{
			esPar = true;
		}
		else
		{
			esPar = false;
		}
		return esPar;
	}
	public MStack ordenarPila(MStack ordenado) {
		// TODO Auto-generated method stub

		MQueue<Character> filaParentesis = new MQueue<>();
		MQueue<Character> filaNumeros = new MQueue<>();
		MQueue<Character> filaOperadores = new MQueue<>();

		while (ordenado.size() > 0)
		{
			char actual = (char) ordenado.pop();

			if ( esNumero(actual) )
			{
				filaNumeros.enqueue(actual);
			}
			else if ( esSimbolo(actual))
			{
				filaOperadores.enqueue(actual);
			}
			else if ( actual == '('|| actual == ')' || actual == '[' || actual == ']' )
			{
				filaParentesis.enqueue(actual);
			}
		}

		while (filaNumeros.size()>0)
		{
			ordenado.push(filaNumeros.dequeue());
		}
		while (filaOperadores.size()>0)
		{
			ordenado.push(filaOperadores.dequeue());
		}
		while (filaParentesis.size()>0)
		{
			ordenado.push(filaParentesis.dequeue());
		}

		return ordenado;
	}
}