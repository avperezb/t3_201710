package controller;

import model.data.structures.MStack;
import model.logic.ManejadorExpresiones;

public class Controller {

	private static ManejadorExpresiones model = new ManejadorExpresiones();

	public static boolean expresionBienFormada( String pExpresion )
	{
		return model.expresionBienFormada(pExpresion);
	}

	public static MStack ordenarPila(MStack ordenado) {				
		return model.ordenarPila(ordenado);
	}
}
