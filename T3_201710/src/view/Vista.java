package view;

import java.util.Scanner;

import model.data.structures.MStack;
import controller.Controller;

public class Vista {


	private static void printMenu(){

		System.out.println("Anyella Valeria P�rez Buend�a. 201619743");
		System.out.println("1. Compruebe que la expresi�n est� formulada correctamente.");
		System.out.println("2. Ordene la expresi�n");
		System.out.println("4. Salir");
	}

	public static void main(String[] args){

		printMenu();

		Scanner sc = new Scanner(System.in);

		for(;;){
			int option = sc.nextInt();

			switch(option){

			case 1: System.out.println("Ingrese la expresi�n de b�squeda."); 
			String busqueda = sc.next();
			boolean bienFormada = Controller.expresionBienFormada(busqueda);
			System.out.println("Su expresi�n est� bien formada: "+ bienFormada);
			break;

			case 2:

				System.out.println("Ingrese la expresi�n de b�squeda.");
				String ordenado = sc.next(); 
				int pos = 0;
				MStack<Character> pila = new MStack<>();
				while ( pos < ordenado.length() )
				{
					pila.push(ordenado.charAt(pos));
					pos++;

				}				
				pila = Controller.ordenarPila(pila);
				String infoPila = "";
				while(pila.size() > 0){
					infoPila += pila.pop();
				}				
				System.out.println("La expresi�n ordenada es: "+ infoPila + "\n");
				break;

			case 4: System.out.println("Adi�s  \n---------"); sc.close(); return;		  


			default: System.out.println("--------- \n Invalid option !! \n---------");
			}
		}
	}
}
