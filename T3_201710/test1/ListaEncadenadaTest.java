import java.util.Iterator;
import java.util.Stack;

import model.data.structures.ListaEncadenada;
import model.data.structures.MStack;
import junit.framework.TestCase;


public class ListaEncadenadaTest extends TestCase {

	private ListaEncadenada<String> listaSencilla;

	private Iterator<String> iterador;

	public void setUpEscenario1() {

		listaSencilla = new ListaEncadenada<>();
	}

	public void setUpEscenario2() {

		setUpEscenario1();

		listaSencilla.agregarAlFinal("a");
		listaSencilla.agregarAlFinal("b");
		listaSencilla.agregarAlFinal("c");
		listaSencilla.agregarAlFinal("d");
		listaSencilla.agregarAlFinal("e");

	}

	public void testGetFirst() {

		setUpEscenario2();
		listaSencilla.getFirst();
		assertEquals("No es lo esperado", "a", listaSencilla.getFirst().getItem());
	}
	public void testGetSize() {

		setUpEscenario2();

		listaSencilla.size();

		assertEquals("No es lo esperado", 5, listaSencilla.size());

	}

	public void TestIterador( ) {

		setUpEscenario2();

		iterador = listaSencilla.iterator();

		assertTrue("Deber�a ser verdadero", iterador.hasNext());
		assertEquals("Deber�a ser a", "a", iterador.next());
	}

	public void testAgregarElementoFinal () {

		setUpEscenario1();

		listaSencilla.agregarAlFinal( "a");
		listaSencilla.agregarAlFinal("b");
		listaSencilla.agregarAlFinal("c");

		assertEquals("No es el esperado", "a", listaSencilla.darElemento(0));
		assertEquals("No es el esperado", "b", listaSencilla.darElemento(1));
		assertEquals("No es el esperado", "c", listaSencilla.darElemento(2));

	}

	public void testDarElemento() {

		setUpEscenario2();

		assertEquals("b", listaSencilla.darElemento(1));
		assertEquals("c", listaSencilla.darElemento(2));
		assertEquals("d", listaSencilla.darElemento(3));
		assertEquals("e", listaSencilla.darElemento(4));

	}

	public void testDarNumeroElementos () {

		setUpEscenario2();

		assertEquals(5, listaSencilla.darNumeroElementos());
	}

	public void testDarElementoPosicionActual () {

		setUpEscenario1();
		assertNull("no deber�a funcionar", listaSencilla.darElementoPosicionActual());
		setUpEscenario2();
		assertEquals("Deber�a retornar a", "a", listaSencilla.darElementoPosicionActual());

	}

	public void testAvanzarSiguientePosicion () {

		setUpEscenario1();
		assertFalse("No debe retornar true", listaSencilla.avanzarSiguientePosicion());

		setUpEscenario2();
		assertTrue("Deber�a retornar true", listaSencilla.avanzarSiguientePosicion());

	}

	public void testRetrocederPosicionAnterior () {

		setUpEscenario1();
		assertFalse("deber�a retornar false, no hay hacia donde retroceder", listaSencilla.retrocederPosicionAnterior());

		setUpEscenario2();
		assertTrue("Deber�a retroceder una posicion", listaSencilla.retrocederPosicionAnterior());
	}

}
