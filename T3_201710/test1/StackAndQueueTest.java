import junit.framework.TestCase;
import model.data.structures.ListaEncadenada;
import model.data.structures.MQueue;
import model.data.structures.MStack;


public class StackAndQueueTest extends TestCase {

	private MStack<String> stack;

	private MQueue<String> queue;

	public void setUpEscenario3()
	{
		stack = new MStack<>();	

	}

	public void setUpEscenario4() {

		setUpEscenario3();

		stack.push("a");
		stack.push("b");
		stack.push("c");
		stack.push("d");


	}

	public void setUpEscenario5()
	{
		queue = new MQueue<>();
	}

	public void setUpEscenario6()
	{
		setUpEscenario5();
		queue.enqueue("primero");
		queue.enqueue("segundo");
		queue.enqueue("tercero");
		queue.enqueue("cuarto");
		queue.enqueue("quinto");
	}

	public void testPop()
	{
		setUpEscenario3();

		stack.push("b");
		stack.push("c");
		stack.push("d");		
		assertEquals( "No es lo esperado", "d", stack.pop());

	}

	public void testPush() {

		setUpEscenario3();

		stack.push("b");
		stack.push("c");
		stack.push("d");
		stack.push("e");
		stack.push("f");
		stack.push("g");

		assertEquals("b", stack.darElemento(5));
		assertEquals("g", stack.darElemento(0));

	}

	public void testSize() {

		setUpEscenario4();

		assertEquals(4 , stack.size());
	}

	public void testIsEmpty()
	{
		setUpEscenario3();
		assertEquals(0, stack.size());


		setUpEscenario4();
		assertEquals(4, stack.size());
	}

	public void testEnqueue()
	{
		setUpEscenario5();

		queue.enqueue("primero");
		queue.enqueue("quinto");
		queue.enqueue("hola");

		assertEquals("hola", queue.darElemento(2));
		assertEquals("primero", queue.darElemento(0));

	}

	public void testQIsEmpty()
	{
		setUpEscenario5();
		queue.isEmpty();
		assertTrue(true);

		setUpEscenario6();
		queue.enqueue("elemento");
		assertFalse(false);
	}

	public void testDequeue()
	{

		setUpEscenario6();
		assertEquals("primero", queue.dequeue());

	}

	public void testQSize()
	{
		setUpEscenario5();
		assertEquals(0, queue.size());


		setUpEscenario6();
		assertEquals(5, queue.size());

	}
}